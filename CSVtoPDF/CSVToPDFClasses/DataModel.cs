﻿using System;
using System.Collections.Generic;
using System.Text;


namespace CSVtoPDF.CSVToPDFClasses
{
    class DataModel
    {
        
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string orderNumber { get; set; }
        public DateTime orderOnDate { get; set; }
        public string email { get; set; }
        public string guestList { get; set; }
        public string product{ get; set; }
        public string ticketName { get; set; }
        public string ean { get; set; }
        public string ticketStatus { get; set; }
        public string ticketFirstName { get; set; }
        public string ticketLastName { get; set; }
        public string ticketEmail { get; set; }
        public string ticketPhoneNumber { get; set; }
        public TimeSpan ticketEntranceTime { get; set; }
        public string leadingValue{ get; set; }

    }
}
