﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.IO;
using iText;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Kernel.Pdf.Action;
using iText.Layout.Borders;

namespace CSVtoPDF.CSVToPDFClasses
{
    class Worker
    {
        DataModel model;

        public Worker() 
        {
            model = new DataModel();
        }

        public bool ParseCSV(string pathToCSV) 
        {
            int counter = 0;

            try
            {
                using (var reader = new StreamReader(pathToCSV))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(';');

                       
                        if (counter > 0)
                        {
                            model = new DataModel();
                            model.lastName = values[0];
                            model.firstName = values[1];
                            model.orderNumber = values[2];
                            model.orderOnDate = new DateTime(Int32.Parse(values[3].Split(" ")[0].Split("-")[2]), Int32.Parse(values[3].Split(" ")[0].Split("-")[1]), Int32.Parse(values[3].Split(" ")[0].Split("-")[0]));
                            model.email = values[4];
                            model.guestList = values[5];
                            model.product = values[6];
                            model.ticketName = values[7];
                            model.ean = values[8];
                            model.ticketStatus = values[9];
                            model.ticketFirstName = values[10];
                            model.ticketLastName = values[11];
                            model.ticketEmail = values[12];
                            model.ticketPhoneNumber = values[13];
                            model.ticketEntranceTime = values[14] != null && values[14] != "" ? new TimeSpan(Int32.Parse(values[14].Split(":")[0]), Int32.Parse(values[14].Split(":")[1]), Int32.Parse(values[14].Split(":")[2])) : new TimeSpan(0,0,0);
                            model.leadingValue = values[12];
                            InsertCSVDataIntoDB();
                        }
                        else 
                        {
                            counter++;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public void InsertCSVDataIntoDB()
        {
            try 
            {
                string connectionString = "Data Source=DESKTOP-67RE7UF\\SQLEXPRESS;Initial Catalog=HelloWorld;Integrated Security=True";

                string createTableIfNotExists = "if not exists(select * from sysobjects where name = 'CSVImportForPDF' and xtype = 'U')" +
                    "CREATE TABLE CSVImportForPDF" +
                    "(" +
                    "    lastName varchar(255)," +
                    "    firstName varchar(255)," +
                    "    orderNumber varchar(255)," +
                    "    orderOnDate datetime," +
                    "    email varchar(255)," +
                    "    guestList varchar(255)," +
                    "    product varchar(255)," +
                    "    ticketName varchar(255)," +
                    "    ean varchar(255)," +
                    "    ticketStatus varchar(255)," +
                    "    ticketFirstName varchar(255)," +
                    "    ticketLastName varchar(255)," +
                    "    ticketEmail varchar(255)," +
                    "    ticketPhoneNumber varchar(255)," +
                    "    ticketEntranceTime time," +
                    "    leadingValue varchar(255)," +
                    ")";

                string insertStatment = "INSERT INTO CSVImportForPDF (lastName, firstName, orderNumber, orderOnDate" +
                    ", email, guestList, product, ticketName, ean, ticketStatus, ticketFirstName, ticketLastName" +
                    ", ticketEmail, ticketPhoneNumber, ticketEntranceTime, leadingValue) " +
                    "VALUES " +
                    "(" +
                    AddSingleQuotes(model.lastName) + "," +
                    AddSingleQuotes(model.firstName) + "," +
                    AddSingleQuotes(model.orderNumber) + "," +
                    AddSingleQuotes(model.orderOnDate) + "," +
                    AddSingleQuotes(model.email) + "," +
                    AddSingleQuotes(model.guestList) + "," +
                    AddSingleQuotes(model.product) + "," +
                    AddSingleQuotes(model.ticketName) + "," +
                    AddSingleQuotes(model.ean) + "," +
                    AddSingleQuotes(model.ticketStatus) + "," +
                    AddSingleQuotes(model.ticketFirstName) + "," +
                    AddSingleQuotes(model.ticketLastName) + "," +
                    AddSingleQuotes(model.ticketEmail) + "," +
                    AddSingleQuotes(model.ticketPhoneNumber) + "," +
                    AddSingleQuotes(model.ticketEntranceTime) + "," +
                    AddSingleQuotes(model.leadingValue) +
                    ")";

                using (var connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(createTableIfNotExists, connection);
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }


                using (var connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(insertStatment, connection);
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }

            
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void CreatePDF(string path) 
        {

            //Create PDF and save location
            PdfWriter writer = new PdfWriter(path + "\\demo.pdf");
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            // New line
            Paragraph newline = new Paragraph(new Text("\n"));
            document.Add(newline);
            document.Add(newline);

            // Add image
            Image img = new Image(ImageDataFactory
               .Create(@"../../../../Images/1.png"))
               .SetHorizontalAlignment(HorizontalAlignment.CENTER)
               .SetTextAlignment(TextAlignment.CENTER);
            img.SetWidth(150);
            img.SetHeight(75);
            document.Add(img);

            Paragraph subheader = new Paragraph("The analysis for Covid-19 Kliniek are done by an ISO Certified and validated laboratory by the Ministry of Health, Welfare and Sport of The Netherlands for COVID - 19 PCR based diagnostics.")
            .SetTextAlignment(TextAlignment.CENTER)
            .SetFontSize(8);
            document.Add(subheader);


            Paragraph header = new Paragraph("PCR TEST SARS-COV-2 (COVID-19)")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(16);
            document.Add(header);


            document.Add(newline);
            // Line separator

            List<string> headers = new List<string>();
            headers.Add("Date:");
            headers.Add("Name");
            headers.Add("Date of birth");
            headers.Add("Passport number");
            headers.Add("Results of sample number:");
            headers.Add("Test");
            headers.Add("Unique Sample code:");
            headers.Add("Sample taken on:");
            headers.Add("Date received in lab:");
            headers.Add("Date result:");
            headers.Add("Result:");

            Image esign = new Image(ImageDataFactory
              .Create(@"../../../../Signatures/1.png"))
              .SetHorizontalAlignment(HorizontalAlignment.LEFT)
              .SetTextAlignment(TextAlignment.LEFT);
            esign.SetWidth(150);
            esign.SetHeight(75);



            Table table = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
            //table.SetWidth(500);
            //table.SetHeight(370);
            table.SetVerticalBorderSpacing(1);


            var borderLength = 0.5f;

            Cell cellCol1 = new Cell(1, 1);
            Cell cellCol2 = new Cell(1, 1);
            var counter = 0;

            foreach (var v in headers) 
            {
                if (counter == 0) 
                {
                    cellCol1 = new Cell(1, 1)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetBorder(Border.NO_BORDER)
                    .SetPadding(5)
                    ;

                    cellCol2 = new Cell(1, 1)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetBorder(Border.NO_BORDER)
                    .SetPadding(5);

                    cellCol1.SetBorderLeft(new SolidBorder(borderLength)).Add(new Paragraph(v));
                    cellCol1.SetBorderTop(new SolidBorder(borderLength));
                    cellCol2.SetBorderRight(new SolidBorder(borderLength)).Add(new Paragraph(v + "text"));
                    cellCol2.SetBorderTop(new SolidBorder(borderLength));

                    table.AddCell(cellCol1);
                    table.AddCell(cellCol2);


                }

                    else 
                {
                    cellCol1 = new Cell(1, 1)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetBorder(Border.NO_BORDER)
                    .SetPadding(5)
                    ;

                    cellCol2 = new Cell(1, 1)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .SetBorder(Border.NO_BORDER)
                    .SetPadding(5);

                    cellCol1.SetBorderLeft(new SolidBorder(borderLength)).Add(new Paragraph(v));
                    cellCol2.SetBorderRight(new SolidBorder(borderLength)).Add(new Paragraph(v + "text"));
                    table.AddCell(cellCol1);
                    table.AddCell(cellCol2);
                }


                counter++;
            }

            document.Add(newline);
            document.Add(table);

           

            Table footer = new Table(UnitValue.CreatePercentArray(2)).UseAllAvailableWidth();
            table.SetVerticalBorderSpacing(1);


            Cell cellColfooterF1 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);
            Cell cellColfooterF2 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);

            
            cellColfooterF1.Add(esign).SetBorderLeft(new SolidBorder(borderLength)).Add(new Paragraph("Dr. Rambharose"));
            cellColfooterF2.Add(new Paragraph()).SetBorderRight(new SolidBorder(borderLength)).SetBorder(Border.NO_BORDER);


            footer.AddCell(cellColfooterF1);
            footer.AddCell(cellColfooterF2);


            cellColfooterF1 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);
            cellColfooterF2 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);

            Paragraph Foottersubheader = new Paragraph("The analysis for Covid-19 Kliniek are done by an ISO Certified and validated laboratory by the Ministry of Health, Welfare and Sport of The Netherlands for COVID - 19 PCR based diagnostics.")
            .SetTextAlignment(TextAlignment.CENTER)
            .SetFontSize(8);

            cellColfooterF1.SetBorderLeft(new SolidBorder(borderLength)).Add(Foottersubheader);
            cellColfooterF2.Add(new Paragraph()).SetBorderRight(new SolidBorder(borderLength)).SetBorder(Border.NO_BORDER);

            footer.AddCell(cellColfooterF1);
            footer.AddCell(cellColfooterF2);


            cellColfooterF1 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);
            cellColfooterF2 = new Cell(1, 1).SetBorder(Border.NO_BORDER).SetPadding(5);

            Image footerImage = new Image(ImageDataFactory
              .Create(@"../../../../Images/footer.png"))
              .SetHorizontalAlignment(HorizontalAlignment.LEFT)
              .SetTextAlignment(TextAlignment.LEFT);
            footerImage.SetWidth(150);
            footerImage.SetHeight(75);


            cellColfooterF1.Add(footerImage).SetBorderLeft(new SolidBorder(borderLength)).SetBorderBottom(new SolidBorder(borderLength)).SetBorder(Border.NO_BORDER);
            cellColfooterF2.Add(new Paragraph()).SetBorderRight(new SolidBorder(borderLength)).SetBorderBottom(new SolidBorder(borderLength)).SetBorder(Border.NO_BORDER);

            footer.AddCell(cellColfooterF1);
            footer.AddCell(cellColfooterF2);


            document.Add(footer);



            
            //document.Add(Foottersubheader);
            

            
            //document.Add(footerImage);



            // Page numbers
            int n = pdf.GetNumberOfPages();
            for (int i = 1; i <= n; i++)
            {
                document.ShowTextAligned(new Paragraph(String
                   .Format("page" + i + " of " + n)),
                   559, 806, i, TextAlignment.RIGHT,
                   VerticalAlignment.BOTTOM, 0);
            }

            // Close document
            document.Close();




        }

        public string AddSingleQuotes(string value) 
        {
            if(value == "") { return "''" ; }
            return "'" + value + "'";
        }

        public string AddSingleQuotes(DateTime value)
        {
            return string.Format("'{0}'", value.ToString());
        }

        public string AddSingleQuotes(TimeSpan value)
        {
            return string.Format("'{0}'", value.ToString());
        }

    }
}
