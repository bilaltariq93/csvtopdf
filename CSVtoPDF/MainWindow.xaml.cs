﻿using CSVtoPDF.CSVToPDFClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CSVtoPDF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Worker work = new Worker();

            var readCSV = work.ParseCSV(___txtBox_PathToCSV_.Text);
            MessageBox.Show("Successfully saved in the Database");
            work.CreatePDF("D:\\Git Projects\\CSVPDF\\CSVtoPDF\\PDFs");
            MessageBox.Show("PDF Created");

        }

        private void ___txtBox_PathToCSV__TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}